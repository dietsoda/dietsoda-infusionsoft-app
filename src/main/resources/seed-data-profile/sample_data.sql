-- The order of deletion is important due to FK constraints.


DELETE FROM infusionsoft_user;
DELETE FROM infusionsoft_xmlrpc_api_key;
DELETE FROM infusionsoft_application_instance;

DELETE FROM user;



INSERT INTO user (id, email_address, name) VALUES (111, "wschultz@dietsodasoftware.com", "Wendel");

-- wendel
INSERT INTO infusionsoft_application_instance(id, app_id) VALUES (222, "ab123");
INSERT INTO infusionsoft_xmlrpc_api_key(id, access_key, infusionsoft_application_instance_id) VALUES (333, 'ab123-api-key', 222);
INSERT INTO infusionsoft_user(id, infusionsoft_user_id, infusionsoft_application_instance_id, user_id) VALUES (444, 1, 222, 111);


-- Don't commit stuff below here


