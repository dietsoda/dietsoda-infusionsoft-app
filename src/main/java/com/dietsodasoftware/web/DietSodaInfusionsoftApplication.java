package com.dietsodasoftware.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.dietsodasoftware.web"})
@SpringBootApplication
public class DietSodaInfusionsoftApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(DietSodaInfusionsoftApplication.class, args);
    }

    /**
     *  This allows the war file to deploy directly into tomcat.
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DietSodaInfusionsoftApplication.class);
    }
    
}
