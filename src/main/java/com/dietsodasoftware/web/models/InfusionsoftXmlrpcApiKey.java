package com.dietsodasoftware.web.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Created by wendel.schultz on 1/29/16.
 */
@Entity
public class InfusionsoftXmlrpcApiKey {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "access_key")
    private String accessKey;

    @OneToOne
    @JoinColumn(name = "infusionsoft_application_instance_id")
    private InfusionsoftApplicationInstance infusionsoftApplicationInstance;

    public long getId() {
        return id;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public InfusionsoftApplicationInstance getInfusionsoftApplicationInstance() {
        return infusionsoftApplicationInstance;
    }

    public void setInfusionsoftApplicationInstance(InfusionsoftApplicationInstance infusionsoftApplicationInstance) {
        this.infusionsoftApplicationInstance = infusionsoftApplicationInstance;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("accessKey", accessKey)
                .append("infusionsoftApplicationInstance", infusionsoftApplicationInstance.getAppId())
                .toString();
    }
}
