package com.dietsodasoftware.web.models;

import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.ZonedDateTime;

/**
 * Created by wendel.schultz on 2/4/16.
 */
@Entity
public class InfusionsoftOauthBearerToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    @JoinColumn(name = "infusionsoft_user_id")
    private InfusionsoftUser infusionsoftUser;

    @Column(name ="access_token")
    private String accessToken;

    @Column(name = "expires", nullable = false, columnDefinition = "TIMESTAMP")
    @Type( type = "org.jadira.usertype.dateandtime.threeten.PersistentZonedDateTime" )
    private ZonedDateTime expires;

    @Column(name ="refresh_token")
    private String refreshToken;

    @Column(name ="scope")
    private String scope;

    @Column(name = "creation", nullable = false, columnDefinition = "TIMESTAMP")
    @CreatedDate
    @Type( type = "org.jadira.usertype.dateandtime.threeten.PersistentZonedDateTime" )
    private ZonedDateTime creation;


    public long getId() {
        return id;
    }

    public InfusionsoftUser getInfusionsoftUser() {
        return infusionsoftUser;
    }

    public void setInfusionsoftUser(InfusionsoftUser user) {
        this.infusionsoftUser = user;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    public void setExpires(ZonedDateTime expires) {
        this.expires = expires;
    }

    public ZonedDateTime getCreation() {
        return creation;
    }

    public void setCreation(ZonedDateTime creation) {
        this.creation = creation;
    }
}
