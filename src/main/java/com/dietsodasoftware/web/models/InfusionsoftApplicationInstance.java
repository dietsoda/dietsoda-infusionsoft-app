package com.dietsodasoftware.web.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Created by wendel.schultz on 1/29/16.
 */
@Entity
public class InfusionsoftApplicationInstance {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "app_id")
    private String appId;

    @OneToOne(mappedBy = "infusionsoftApplicationInstance")
    private InfusionsoftXmlrpcApiKey accessToken;

    public long getId() {
        return id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public InfusionsoftXmlrpcApiKey getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(InfusionsoftXmlrpcApiKey accessToken) {
        this.accessToken = accessToken;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("appId", appId)
                .append("accessToken", accessToken)
                .toString();
    }
}
