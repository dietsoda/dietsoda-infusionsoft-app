package com.dietsodasoftware.web.models;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private String name;

    @Column(unique = true)
    private String emailAddress;

    @OneToOne(mappedBy = "user")
    private InfusionsoftUser infusionsoftUser;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String nName) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public InfusionsoftUser getInfusionsoftUser() {
        return infusionsoftUser;
    }

    public void setInfusionsoftUser(InfusionsoftUser infusionsoftUser) {
        this.infusionsoftUser = infusionsoftUser;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("id", id)
                .append("name", name)
                .append("emailAddress", emailAddress)
                .append("infusionsoftUser", infusionsoftUser)
                .toString();
    }

}
