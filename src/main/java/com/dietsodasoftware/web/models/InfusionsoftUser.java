package com.dietsodasoftware.web.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * An infusionsoft user has a natural compound primary key across appId and userId.
 */
@Entity
@Table(
        uniqueConstraints={
                @UniqueConstraint(columnNames={"infusionsoft_user_id", "infusionsoft_application_instance_id"})
        }

)
public class InfusionsoftUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "infusionsoft_user_id")
    private Long infusionsoftUserId;

    @OneToOne(mappedBy = "infusionsoftUser")
    private InfusionsoftOauthBearerToken oauthToken;


    @ManyToOne(optional = false)
    @JoinColumn(name = "infusionsoft_application_instance_id")
    private InfusionsoftApplicationInstance infusionsoftApplicationInstance;

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public InfusionsoftApplicationInstance getInfusionsoftApplicationInstance() {
        return infusionsoftApplicationInstance;
    }

    public void setInfusionsoftApplicationInstance(InfusionsoftApplicationInstance infusionsoftApplicationInstance) {
        this.infusionsoftApplicationInstance = infusionsoftApplicationInstance;
    }

    public Long getInfusionsoftUserId() {
        return infusionsoftUserId;
    }

    public void setInfusionsoftUserId(Long infusionsoftUserId) {
        this.infusionsoftUserId = infusionsoftUserId;
    }

    public InfusionsoftOauthBearerToken getOauthToken() {
        return oauthToken;
    }

    public void setOauthToken(InfusionsoftOauthBearerToken oauthToken) {
        this.oauthToken = oauthToken;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("userId", user.getId())
                .append("infusionsoftApplicationInstance", infusionsoftApplicationInstance)
                .append("infusionsoftUserId", infusionsoftUserId)
                .toString();
    }
}
