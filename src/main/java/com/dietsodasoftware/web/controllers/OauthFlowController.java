package com.dietsodasoftware.web.controllers;

import com.dietsodasoftware.web.services.infusionsoft.InfusionsoftOauth2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by wendel.schultz on 2/2/16.
 */
@Controller
public class OauthFlowController {

    @Autowired
    private InfusionsoftOauth2Service oauth2Service;


    @RequestMapping(value = "/auth", method = RequestMethod.GET)
    public String auth() {

        return "redirect:" + oauth2Service.createOauthSigninRedirectUri().toString();

    }


}
