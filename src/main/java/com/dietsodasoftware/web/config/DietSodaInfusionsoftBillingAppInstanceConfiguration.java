package com.dietsodasoftware.web.config;

import com.dietsodasoftware.yail.xmlrpc.client.YailProfile;
import com.dietsodasoftware.yail.xmlrpc.client.YailProfileBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by wendel.schultz on 1/29/16.
 */
@Configuration
public class DietSodaInfusionsoftBillingAppInstanceConfiguration {

    @Value("${dietsoda-infusionsoft.appname}")
    private String appName;

    @Value("${dietsoda-infusionsoft.apikey")
    private String apiKey;


    @Bean
    @Qualifier("InfusionsoftBillingProfile")
    public YailProfile yailProfile(){
        // you could also use OAuth here if you store the token and refresh it, etc.
        return new YailProfileBuilder(appName)
                .usingApiKey(apiKey)
                .build();
    }

}
