package com.dietsodasoftware.web.api.utils;

import com.dietsodasoftware.web.api.exceptions.BadRequestException;

/**
 * Very much like Preconditions, but throw an exception resulting in a 400, not 500.
 */
public class ParameterConditions {

    public static void checkNotNull(Object condition, String messageIfNull) {
        if(condition == null){
            throw new BadRequestException(messageIfNull);
        }
    }
}
