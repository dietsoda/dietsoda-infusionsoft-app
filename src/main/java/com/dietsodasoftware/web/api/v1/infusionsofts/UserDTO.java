package com.dietsodasoftware.web.api.v1.infusionsofts;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by wendel.schultz on 1/30/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    public Long id;

    public String name;

    public String emailAddress;

    public Long infusionsoftUserId;

    public String infusionsoftAppName;

}
