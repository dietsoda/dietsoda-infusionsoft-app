package com.dietsodasoftware.web.api.v1.infusionsofts;

import com.dietsodasoftware.web.models.User;
import org.springframework.stereotype.Component;

/**
 * Created by wendel.schultz on 1/30/16.
 */
@Component
class UserTransformer {

    UserDTO toDto(User user) {
        UserDTO dto = new UserDTO();

        dto.id = user.getId();
        dto.name = user.getName();
        dto.emailAddress = user.getEmailAddress();
        dto.infusionsoftAppName = user.getInfusionsoftUser().getInfusionsoftApplicationInstance().getAppId();
        dto.infusionsoftUserId = user.getInfusionsoftUser().getInfusionsoftUserId();

        return dto;
    }

    UserListDTO toDto(UserDTO ... users) {
        UserListDTO dto = new UserListDTO();

        if(users != null) {
            for(UserDTO udto: users) {
                dto.users.add(udto);
            }
            dto.count = users.length;
        }

        return dto;

    }
}
