package com.dietsodasoftware.web.api.v1.infusionsofts;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by wendel.schultz on 1/30/16.
 */
public class UserListDTO {

    public List<UserDTO> users = Lists.newLinkedList();

    public int count = 0;

}
