package com.dietsodasoftware.web.api.v1.infusionsofts;

import com.dietsodasoftware.web.api.exceptions.BadRequestException;
import com.dietsodasoftware.web.api.exceptions.NotFoundException;
import com.dietsodasoftware.web.models.InfusionsoftUser;
import com.dietsodasoftware.web.models.User;
import com.dietsodasoftware.web.services.UserService;
import com.dietsodasoftware.yail.xmlrpc.service.InfusionsoftParameterValidationException;
import com.dietsodasoftware.yail.xmlrpc.service.InfusionsoftXmlRpcException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wendel.schultz on 1/30/16.
 */
@RestController
public class InfusionsoftApplicationInstanceController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserTransformer userTransformer;


    @RequestMapping(value = "/api/v1/infusionsofts/{app_name}/users", method = RequestMethod.GET)
    public UserListDTO loadInfusionsoftUser(@PathVariable("app_name") String appName,
                                            @RequestParam(value = "email", required = true) String email)
    {
        final UserListDTO users;

        if(email != null) {
            users = loadSingleAppUserByEmail(appName, email);
        } else {
            throw new BadRequestException("Must request user by providing email address parameter: 'email'");
        }

        return users;
    }



    private UserListDTO loadSingleAppUserByEmail(String appName, String email) {
        final User user;
        try {
            InfusionsoftUser infusionsoftUser = userService.findInfusionsoftAppUser(appName, email);

            if(infusionsoftUser == null) {
                user = null;
            } else {
                user = infusionsoftUser.getUser();
            }
        } catch (InfusionsoftXmlRpcException e) {
            throw new BadRequestException(e.getMessage());
        } catch (InfusionsoftParameterValidationException e) {
            throw new BadRequestException(e.getMessage());
        }

        if (user == null) {
            throw new NotFoundException("User not found: " + email + " for app " + appName);
        }

        UserDTO udto = userTransformer.toDto(user);
        return userTransformer.toDto(udto);
    }

}
