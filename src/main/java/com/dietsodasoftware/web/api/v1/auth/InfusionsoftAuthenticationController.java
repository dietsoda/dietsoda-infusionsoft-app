package com.dietsodasoftware.web.api.v1.auth;

import com.dietsodasoftware.yail.oauth2.client.tokens.AuthenticationCodeContext;
import com.dietsodasoftware.yail.oauth2.client.tokens.OauthBearerToken;
import com.dietsodasoftware.web.api.utils.ParameterConditions;
import com.dietsodasoftware.web.services.UserService;
import com.dietsodasoftware.web.services.infusionsoft.InfusionsoftOauth2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by wendel.schultz on 2/2/16.
 *
 * I get this:
 *
 state: [1de03c35-11a8-48c7-b53e-df2be4d1cdba]
 scope: [full|zt119.infusionsoft.com]
 code: [g7c84sk6f3rtufzr6p5z4fkk]
 */
@Controller
public class InfusionsoftAuthenticationController {

    @Autowired
    private InfusionsoftOauth2Service oauth2Service;

    @Autowired
    private UserService userService;

    // HATEOAS link creation enforces these validations, while not needing/using them.
    // Here, allow the params to be null (for linkTo(methodOn()) ), but checkNotNull immediately.
    @RequestMapping(value = "/api/v1/infusionsoft_authentication/handle_authentication_code")
    public ModelAndView handleAuthenticationCodeCallback(HttpServletRequest originalRequest,
                                            @RequestParam(value = "state", required = false) String state,
                                            @RequestParam(value = "code", required = false) String code,
                                            @RequestParam(value = "scope", required = false) String scope
                                            ) {

        ParameterConditions.checkNotNull(state, "state must not be null");
        ParameterConditions.checkNotNull(code, "code must not be null");
        ParameterConditions.checkNotNull(scope, "scope must not be null");

        final String requestUri = originalRequest.getRequestURL().toString();
        AuthenticationCodeContext codeContext = new AuthenticationCodeContext(scope, state, code, requestUri);
        System.err.println("  Context: " + codeContext);

        try {

            // how to create a grant ticket
            OauthBearerToken token = oauth2Service.createGrantTicketFromAuthCode(codeContext);


            // how to refresh a ticket
            OauthBearerToken refresh = oauth2Service.refreshBearerToken(token.getRefreshToken());
            System.err.println("  Refresh Token:  " + refresh);


        } catch (IOException e) {
            throw new RuntimeException("Unable to complete OAuth flow with Infusionsoft. " + e.getMessage());
        }

        // go wherever makes sense.
        return null;
    }

}
