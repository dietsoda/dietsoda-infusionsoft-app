package com.dietsodasoftware.web.services;

import com.dietsodasoftware.web.services.infusionsoft.InfusionsoftApplicationInstanceService;
import com.dietsodasoftware.yail.xmlrpc.service.InfusionsoftParameterValidationException;
import com.dietsodasoftware.yail.xmlrpc.service.InfusionsoftXmlRpcException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wendel.schultz on 2/1/16.
 */
@Service
public class CampaignGoalService {

    @Autowired
    private InfusionsoftApplicationInstanceService applicationInstanceService;


    public Void achieveCampaignGoal(String appName, String goalName, int contactId) throws InfusionsoftXmlRpcException, InfusionsoftParameterValidationException {
        applicationInstanceService.completeApiGoal(appName, goalName, contactId);

        return null;
    }

}
