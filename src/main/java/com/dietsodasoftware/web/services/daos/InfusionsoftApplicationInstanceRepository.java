package com.dietsodasoftware.web.services.daos;

import com.dietsodasoftware.web.models.InfusionsoftApplicationInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by wendel.schultz on 1/29/16.
 */
@Repository
public interface InfusionsoftApplicationInstanceRepository extends JpaRepository<InfusionsoftApplicationInstance, Long> {

    public InfusionsoftApplicationInstance findByAppId(@Param("appId") String appName);

}
