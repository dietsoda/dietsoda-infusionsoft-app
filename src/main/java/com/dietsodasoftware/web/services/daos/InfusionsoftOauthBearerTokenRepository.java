package com.dietsodasoftware.web.services.daos;

import com.dietsodasoftware.web.models.InfusionsoftOauthBearerToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wendel.schultz on 2/4/16.
 */
@Repository
public interface InfusionsoftOauthBearerTokenRepository extends JpaRepository<InfusionsoftOauthBearerToken, Long> {

}
