package com.dietsodasoftware.web.services.daos;

import com.dietsodasoftware.web.models.InfusionsoftUser;
import com.dietsodasoftware.web.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by wendel.schultz on 1/28/16.
 */
@Repository
public interface InfusionsoftUserRepository extends JpaRepository<InfusionsoftUser, Long> {

    @Query("SELECT iu FROM InfusionsoftUser iu JOIN iu.infusionsoftApplicationInstance app WHERE app.appId = :appId AND iu.infusionsoftUserId = :userId")
    public InfusionsoftUser findByInfusionsoftIdentifiers(@Param("appId") String infusionsoftAppName, @Param("userId") Long userId);

    @Query("SELECT u FROM InfusionsoftUser iu JOIN iu.user u JOIN iu.infusionsoftApplicationInstance app WHERE app.appId = :appId ")
    public List<User> findByInfusionsoftAppName(@Param("appId") String infusionsoftAppName);
}
