package com.dietsodasoftware.web.services.daos;

import com.dietsodasoftware.web.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by wendel.schultz on 1/28/16.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u JOIN u.infusionsoftUser iu JOIN iu.infusionsoftApplicationInstance app WHERE app.appId = :appId AND iu.infusionsoftUserId = :userId")
    public User findUserByInfusionsoftIdentifiers(@Param("appId") String infusionsoftAppName, @Param("userId") Long userId);

}
