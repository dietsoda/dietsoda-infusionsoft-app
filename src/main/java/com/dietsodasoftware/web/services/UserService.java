package com.dietsodasoftware.web.services;

import com.dietsodasoftware.web.models.InfusionsoftOauthBearerToken;
import com.dietsodasoftware.web.models.InfusionsoftUser;
import com.dietsodasoftware.web.models.User;
import com.dietsodasoftware.yail.oauth2.client.tokens.OauthBearerToken;
import com.dietsodasoftware.yail.xmlrpc.model.InfusionsoftUserInfo;
import com.dietsodasoftware.yail.xmlrpc.service.InfusionsoftParameterValidationException;
import com.dietsodasoftware.yail.xmlrpc.service.InfusionsoftXmlRpcException;
import com.dietsodasoftware.web.services.daos.InfusionsoftOauthBearerTokenRepository;
import com.dietsodasoftware.web.services.daos.InfusionsoftUserRepository;
import com.dietsodasoftware.web.services.infusionsoft.InfusionsoftApplicationInstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by wendel.schultz on 1/29/16.
 */
@Service
public class UserService {

    @Autowired
    private InfusionsoftApplicationInstanceService appInstanceService;

    @Autowired
    private InfusionsoftUserRepository infusionsoftUserRepository;

    @Autowired
    private InfusionsoftOauthBearerTokenRepository tokenRepository;


    public InfusionsoftUser findInfusionsoftAppUser(String appName, String email) throws InfusionsoftXmlRpcException, InfusionsoftParameterValidationException {
        com.dietsodasoftware.yail.xmlrpc.model.User isftUser = appInstanceService.findInfusionsoftUserByEmailAddress(appName, email);

        if(isftUser == null){
            throw new IllegalArgumentException("User not found");
        }

        final Integer userId = isftUser.getFieldValue(com.dietsodasoftware.yail.xmlrpc.model.User.Field.Id);

        if(userId == null){
            throw new IllegalArgumentException("User does not have ID");
        }

        return infusionsoftUserRepository.findByInfusionsoftIdentifiers(appName, userId.longValue());

    }

    public List<User> findInfusionsoftAppUsers(String appName) {

        return infusionsoftUserRepository.findByInfusionsoftAppName(appName);

    }


    public InfusionsoftOauthBearerToken updateUserToken(OauthBearerToken bearerToken) throws InfusionsoftXmlRpcException, InfusionsoftParameterValidationException {

        final InfusionsoftUserInfo userInfo = appInstanceService.findInfusionsoftUserByOauthToken(bearerToken);
        final com.dietsodasoftware.yail.xmlrpc.model.User appUser = appInstanceService.findUserByUserInfo(bearerToken, userInfo);
        final InfusionsoftUser user = findInfusionsoftAppUser(userInfo.getAppAlias(), appUser.getFieldValue(com.dietsodasoftware.yail.xmlrpc.model.User.Field.Email));

        final InfusionsoftOauthBearerToken oldToken = user.getOauthToken();
        final InfusionsoftOauthBearerToken token = new InfusionsoftOauthBearerToken();

        final ZonedDateTime now = ZonedDateTime.now();
        final ZonedDateTime expires = now.plusSeconds(bearerToken.getExpiresIn()).minusHours(2);


        token.setAccessToken(bearerToken.getAccessToken());
        token.setRefreshToken(bearerToken.getRefreshToken());
        token.setCreation(now);
        token.setExpires(expires);
        token.setScope(bearerToken.getRawScope());

        token.setInfusionsoftUser(user);
        user.setOauthToken(token);

        final InfusionsoftOauthBearerToken newToken = tokenRepository.save(token);

        if(oldToken != null) {
            tokenRepository.delete(oldToken);
        }


        return newToken;
    }
}
