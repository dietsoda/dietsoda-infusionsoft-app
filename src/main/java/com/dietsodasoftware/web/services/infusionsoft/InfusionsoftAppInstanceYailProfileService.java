package com.dietsodasoftware.web.services.infusionsoft;

import com.dietsodasoftware.web.models.InfusionsoftApplicationInstance;
import com.dietsodasoftware.web.models.InfusionsoftXmlrpcApiKey;
import com.dietsodasoftware.yail.oauth2.client.InfusionsoftOauthToken;
import com.dietsodasoftware.yail.oauth2.client.tokens.OauthBearerToken;
import com.dietsodasoftware.yail.xmlrpc.client.YailProfile;
import com.dietsodasoftware.yail.xmlrpc.client.YailProfileBuilder;
import org.springframework.stereotype.Service;

/**
 * Package scope this.  Hopefully, Infusionsoft API (YAIL) models and mechanics won't spread too far.
 */
@Service
class InfusionsoftAppInstanceYailProfileService {

    /**
     * Require the app instance entity, rather than the app name.  This will ensure that there is some sort of authentication stored with it,
     * be it API key or an OAUTH2 token.  And, this service stays out of the database and only interfaces with Infusionsoft
     * as a "data store."
     */
    YailProfile createCustomYailProfile(InfusionsoftApplicationInstance app) {
        final InfusionsoftXmlrpcApiKey apiKey = app.getAccessToken();

        if(apiKey == null) {
            return null;
        }

        return createCustomerYailProfile(apiKey);
    }

    YailProfile createCustomerYailProfile(InfusionsoftXmlrpcApiKey key) {
        final String appName = key.getInfusionsoftApplicationInstance().getAppId();
        final String apiKey = key.getAccessKey();

        if(apiKey == null || appName == null) {
            return null;
        }

        return new YailProfileBuilder(appName)
                .usingApiKey(apiKey)
                .build();
    }

    YailProfile createCustomerYailProfile(final OauthBearerToken token) {
        return YailProfile.usingOAuth2Token(new InfusionsoftOauthToken() {
            @Override
            public String getToken() {
                return token.getAccessToken();
            }
        });
    }
}
