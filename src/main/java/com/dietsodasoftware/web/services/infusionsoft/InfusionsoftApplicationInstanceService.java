package com.dietsodasoftware.web.services.infusionsoft;

import com.dietsodasoftware.web.models.InfusionsoftApplicationInstance;
import com.dietsodasoftware.web.services.daos.InfusionsoftApplicationInstanceRepository;
import com.dietsodasoftware.yail.oauth2.client.tokens.OauthBearerToken;
import com.dietsodasoftware.yail.xmlrpc.client.YailProfile;
import com.dietsodasoftware.yail.xmlrpc.model.InfusionsoftUserInfo;
import com.dietsodasoftware.yail.xmlrpc.model.User;
import com.dietsodasoftware.yail.xmlrpc.service.InfusionsoftModelCollectionResults;
import com.dietsodasoftware.yail.xmlrpc.service.InfusionsoftParameterValidationException;
import com.dietsodasoftware.yail.xmlrpc.service.InfusionsoftXmlRpcException;
import com.dietsodasoftware.yail.xmlrpc.service.data.DataServiceFindByFieldOperation;
import com.dietsodasoftware.yail.xmlrpc.service.data.DataServiceGetUserInfoOperation;
import com.dietsodasoftware.yail.xmlrpc.service.data.DataServiceLoadOperation;
import com.dietsodasoftware.yail.xmlrpc.service.funnel.AchieveGoalResponse;
import com.dietsodasoftware.yail.xmlrpc.service.funnel.ApiServiceAchieveGoalOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Our application's semantic gateway into Infusionsoft application instances.
 */
@Service
public class InfusionsoftApplicationInstanceService {

    // this matches the integration name in a HTTP POST in a campaign.
    private static final String INTEGRATION_NAME = "DietSodaSoftwareApplicationTemplate";

    @Autowired
    private InfusionsoftAppInstanceYailProfileService yailProfileService;

    @Autowired
    private InfusionsoftApplicationInstanceRepository appInstanceRepository;



    public User findInfusionsoftUserByEmailAddress(String appName, String email) throws InfusionsoftXmlRpcException, InfusionsoftParameterValidationException {

        final DataServiceFindByFieldOperation<User> finder = new DataServiceFindByFieldOperation(User.class)
                .setFieldCriteria(User.Field.Email, email);

        final YailProfile profile = profileForAppName(appName);

        final InfusionsoftModelCollectionResults<User> them = profile.getClient().call(finder);

        if(them.length() > 1){
            throw new RuntimeException("How do you have two users with the same email address?");
        }

        if(them.length() == 0){
            return null;
        }

        return them.iterator().next();
    }

    public InfusionsoftUserInfo findInfusionsoftUserByOauthToken(OauthBearerToken token) throws InfusionsoftXmlRpcException, InfusionsoftParameterValidationException {
        final YailProfile profile = yailProfileService.createCustomerYailProfile(token);

        DataServiceGetUserInfoOperation userInfoCall = new DataServiceGetUserInfoOperation();

        return profile.getClient().call(userInfoCall);
    }

    public User findUserByUserInfo(OauthBearerToken token, InfusionsoftUserInfo userInfo) throws InfusionsoftXmlRpcException, InfusionsoftParameterValidationException {
        final YailProfile profile = yailProfileService.createCustomerYailProfile(token);

        DataServiceLoadOperation<User, User> loadUser = new DataServiceLoadOperation(User.class, userInfo.getLocalUserId());

        return  profile.getClient().call(loadUser);
    }

    public Void completeApiGoal(String appName, String callName, int contactId) throws InfusionsoftXmlRpcException, InfusionsoftParameterValidationException {

        final YailProfile profile = profileForAppName(appName);

        final ApiServiceAchieveGoalOperation apiGoal = new ApiServiceAchieveGoalOperation(INTEGRATION_NAME, callName, contactId);

        AchieveGoalResponse completed = profile.getClient().call(apiGoal);

        return null;
    }


    private YailProfile profileForAppName(String appName) {

        final InfusionsoftApplicationInstance appInstance = appInstanceRepository.findByAppId(appName);
        if(appInstance == null){
            throw new IllegalArgumentException("must initialize app with api key first");
        }

        return yailProfileService.createCustomYailProfile(appInstance);
    }
}
