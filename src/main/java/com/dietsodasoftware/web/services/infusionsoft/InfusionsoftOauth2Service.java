package com.dietsodasoftware.web.services.infusionsoft;

import com.dietsodasoftware.web.services.ResourceLinkService;
import com.dietsodasoftware.yail.oauth2.InfusionsoftOauth2FlowBroker;
import com.dietsodasoftware.yail.oauth2.client.tokens.AuthenticationCodeContext;
import com.dietsodasoftware.yail.oauth2.client.tokens.OauthBearerToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by wendel.schultz on 2/2/16.
 */
@Service
public class InfusionsoftOauth2Service {

    @Autowired
    private ResourceLinkService linkService;


    @Value("${dietsoda-infusionsoft.mashery.client-id}")
    private String clientId;

    @Value("${dietsoda-infusionsoft.mashery.secret}")
    private String secret;

    private InfusionsoftOauth2FlowBroker flo;

    @PostConstruct
    void init() throws URISyntaxException, UnsupportedEncodingException {

        // let's assume the lib defaults are good enough for us (URIs and code state)
        flo = InfusionsoftOauth2FlowBroker.builder(clientId, secret).build();

    }

    public URI createOauthSigninRedirectUri() {

        return flo.createOauthSigninRedirectUri(linkService.getOauthCodeCallbackLink().getHref());

    }

    public OauthBearerToken createGrantTicketFromAuthCode(AuthenticationCodeContext codeContext) throws IOException {

        return flo.createGrantTicketFromAuthCode(codeContext);

    }

    /**
     * AN example exchange:

     curl -i -X POST -H "Authorization: Basic bndoZzQyanJ6cjk0OTh6ZHhrdXA5d3c5OnFkVU1HN2NGY00=" https://api.infusionsoft.com/token -d 'grant_type=refresh_token&refresh_token=ndacp26m8vhtm3g3bqu2qhvy'
     HTTP/1.1 200 OK
     Cache-Control: no-store
     Content-Type: application/json;charset=UTF-8
     Date: Thu, 04 Feb 2016 16:31:03 GMT
     Pragma: no-cache
     Server: Mashery Proxy
     X-Mashery-Responder: prod-j-worker-us-west-1c-65.mashery.com
     transfer-encoding: chunked
     Connection: keep-alive

     {"access_token":"36aa7gax3exenvdrt8222kyn","token_type":"bearer","expires_in":28800,"refresh_token":"fxfhvvw4r4wsg764zntn7xyr","scope":"full|zt119.infusionsoft.com"}

     */
    public OauthBearerToken refreshBearerToken(String refreshToken) throws IOException {

        return flo.refreshBearerToken(refreshToken);

    }
}
