package com.dietsodasoftware.web.services;

import com.dietsodasoftware.web.api.v1.auth.InfusionsoftAuthenticationController;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by wendel.schultz on 10/7/15.
 *
 * Certain controller request mappings can't be linked to.  Specifically, linkTo can't CGLib a string subclass
 * as the return type for the view name.  So return a ModelAndView instead.
 */
@Service
public class ResourceLinkService {

    public Link getOauthCodeCallbackLink() {

        return ControllerLinkBuilder.linkTo(methodOn(InfusionsoftAuthenticationController.class).handleAuthenticationCodeCallback(null, null, null, null)).withRel("code");

    }

}
