create database dietsoda_application;

drop user 'dietsoda'@'%' ;
drop user 'dietsoda'@'localhost' ;

create user 'dietsoda'@'%' identified by 'application';
create user 'dietsoda'@'localhost' identified by 'application';

grant all privileges on dietsoda_application.* to 'dietsoda'@'%' ;
grant all privileges on dietsoda_application.* to 'dietsoda'@'localhost' ;

flush privileges;

-- be sure to set table engine
-- create table t(...) ENGINE = INNODB