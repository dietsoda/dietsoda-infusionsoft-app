# Infusionsoft template app by Wendel

A simple app to demonstrate how to bootstrap an app using [YAIL](https://bitbucket.org/dietsoda/yail/).
  Also, this application has the Oauth flow baked in already. For additional YAIL usage and mechanics, [see this other project](https://bitbucket.org/dietsoda/yail-spring) or the project itself.

Set up your project for IntelliJ

* ./gradlew idea

Create the database, as described in src/sqls/developer_database_setup.sql.

For development, use the seed-data profile.  This will affect configuration such that the DB will be dropped/created,
the data will be seeded as specified in application-seed-data.properties.  Out of the box, it is src/main/resources/sample_data.sql

Easiest way on a laptop is:

* ./gradlew clean build && java -Dspring.profiles.active=seed-data -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -jar ./build/libs/dietsoda-infusionsoft-app.war