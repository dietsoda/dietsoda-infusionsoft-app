#!/bin/bash

mv /opt/bitnami/apache-tomcat/webapps/dietsoda-infusionsoft-app.war /opt/bitnami/apache-tomcat/webapps/ROOT.war
chown tomcat:tomcat /opt/bitnami/apache-tomcat/webapps/ROOT.war


if [ $? != 0 ]; then
    echo "Unable to change war ownership"
    exit 1
fi

exit 0